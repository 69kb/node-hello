#!/bin/bash

# Copy the root directory to the remote server
scp -r ./* apple@172.20.10.2:/Users/apple/Desktop/

# SSH into the remote server and execute deployment commands
ssh apple@172.20.10.2 << EOF
  cd /Users/apple/Desktop/ && npm install && npm start
  which pm2 || npm install -g pm2
  pm2 restart app.js
EOF
