Setting Up a Basic CI/CD Pipeline with GitLab CI/CD
============

This documentation provides step-by-step instructions on how to set up a basic Continuous Integration/Continuous Deployment (CI/CD) pipeline using GitLab CI/CD. The pipeline will involve deploying a sample web application to a remote server using a Node.js Alpine Docker image.

![App Preview](https://i.imgur.com/As8olcK.png)

---

## Step 1: Create a Sample Web Application

```
   mkdir node_hello
   cd node_hello
   npm init
   touch index.js
```

create a simple web application in `index.js`

---

#### Step 2: Set Up a GitLab Repository

1. Create a new repository on GitLab:

2. Log in to your GitLab account.
   Create a new project and follow the instructions to initialize the repository.
   Push your sample web application code to the GitLab repository:

3. Initialize your local repository and commit your code changes.
   Link your local repository to the remote GitLab repository.
   Push your code to GitLab.

---

## Step 3: Configure the CI/CD Pipeline
1. Create a .gitlab-ci.yml file in the root directory of your repository.
2. Configure the pipeline stages and steps in the .gitlab-ci.yml file:
```
stages:
  - build
  - deploy

build:
  stage: build
  image: node:alpine
  script:
    - echo "Building the application..."
    - npm install

deploy:
  stage: deploy
  script:
    - echo "Deploying application to the remote server..."
    - chmod +x ./deploy_script.sh  
    - ./deploy_script.sh  
  only:
    - main

```
---

## Step 4: Create the Deployment Script
1. Create a deploy_script.sh file in the root directory of your repository.
2. Add the deployment commands to the script:
```
#!/bin/bash

# Copy the root directory to the remote server
scp -r ./* apple@172.20.10.2:/Users/apple/Desktop/

# SSH into the remote server and execute deployment commands
ssh apple@172.20.10.2 << EOF
  cd /path/on/vm && npm install
  which pm2 || npm install -g pm2
  pm2 restart app.js
EOF

```
*** Replace apple@172.20.10.2 with remote server

I employed the use of SCP and SSH to access the remote server for deployment

---

## Step 5: Run the Pipeline
1. Push your .gitlab-ci.yml and deploy_script.sh files to the GitLab repository.
2. Navigate to the repository on GitLab and go to "CI/CD > Pipelines".
3. The pipeline will automatically run when you push to the main branch.
4. Monitor the pipeline's progress and logs in the GitLab interface.

